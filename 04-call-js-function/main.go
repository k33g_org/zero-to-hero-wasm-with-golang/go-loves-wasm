package main

import (
	"fmt"
	"syscall/js"
)

func main() {
	// Call JavaScript function
	fmt.Println("[From Go]", js.Global().Call("sayHello", "Bill"))

	// Get JavaScript value
	message := js.Global().Get("message").String()
	fmt.Println("[From Go]","message (before):", message)

	// Change the JavaScript value
	js.Global().Set("message", "😉 Hello from Go")

	// Get JavaScript value
	bill := js.Global().Get("bill")
	fmt.Println("[From Go]","bill (before):", bill)

	// Change the JavaScript values
	bill.Set("firstName", "Bill")
	bill.Set("lastName", "Ballantine")

	<-make(chan bool)
}
