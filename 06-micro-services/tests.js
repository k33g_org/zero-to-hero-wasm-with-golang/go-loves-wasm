const fs = require('fs')
require("./wasm_exec")

async function runWasm(wasmFile, args) {
  const go = new Go()
  try {
    let { instance } = await WebAssembly.instantiate(wasmFile, go.importObject)
    if(args) go.argv = args
    go.run(instance) 
  } catch(err) {
    throw err
  }
}

const loadWasmFile = async () => {

  const wasmFile = fs.readFileSync('./hello/main.wasm')
  await runWasm(wasmFile)

  callfunction()
}
loadWasmFile()


const callfunction = () => {
  console.log(
    Handle({
      firstname: "Bob",
      lastname: "Morane"
    })
  )
}
