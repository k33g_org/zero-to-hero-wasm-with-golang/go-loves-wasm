# wasm go microservice


```bash
node index.js
```


### Query

```bash
http POST http://0.0.0.0:8080/ firstname="Bob" lastname="Morane"
http POST http://0.0.0.0:8080/ firstname="Bill" lastname="Ballantine"
```

### Query from outside

```bash
# type the command below to get a "public" url
url=$(gp url 8080)
curl \
  -d '{"firstname":"Bill", "lastname":"Ballantine"}' \
  -H "Content-Type: application/json" \
  -X POST ${url}
```
